FROM python:3.10-buster

RUN apt-get update \
    && apt-get install -y openjdk-11-jre-headless git curl \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ADD requirements.txt /app

RUN python -m pip install -U pip

RUN pip3 install -r requirements.txt

ADD cub.py /

ADD . /app

RUN echo "Downloading Avro tools" \
    && sh download_avro-tools.sh

EXPOSE 80

ENV NAME avro-extended

CMD ["./run_code_checks.sh"]
